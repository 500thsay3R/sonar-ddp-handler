/* eslint-env: node, es6 */
/* eslint strict: 0 */
/* eslint no-console: 0 */
/* eslint arrow-body-style: 0 */
/* eslint no-param-reassign: 0 */
/* eslint no-underscore-dangle: 0 */
/* eslint no-return-assign: 0 */
/* eslint wrap-iife: 0 */
/* eslint no-case-declarations: 0 */
'use strict';

require('harmony-reflect');

const WebSocket = require('faye-websocket');
const http = require('http');
const winston = require('winston');

// set up a new logger
const logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({
      prettyPrint: true,
      level: process.env.NODE_ENV,
    }),
  ],
});

/**
 * Create new instance of DDP-server.
 * @param {Object} opts - DDP-server's instantiating options.
 * @param {Object} opts.httpServer - Existing HTTP-server to use side-by-side with DDP-server.
 * @constructor
 */
function DDPServer(opts = {}) {
  let server = opts.httpServer;
  const { heartbeatInterval = 15000, heartbeatTimeout = 30000, port = 3000 } = opts;
  const methods = opts.methods || {};
  const collections = {};
  const subscriptions = {};

  if (!server) {
    server = http.createServer();
    server.listen(port);
  }

  server.on('upgrade', function upgrade(request, socket, body) {
    if (WebSocket.isWebSocket(request)) {
      let ws = new WebSocket(request, socket, body);
      let sessionID = `${new Date().getTime()}`;
      subscriptions[sessionID] = {};

      const sendMessage = (data) => {
        ws.send(JSON.stringify(data));
      };

      /**
       * Performs periodic client ping requests.
       * @param {Function} fn - The ping function.
       * @param {number} interval - Ping interval.
       */
      const keepMeAlive = (fn, interval) => {
        (function tickTack() {
          // check whether the function is still running
          const proceed = !fn();
          if (proceed) {
            setTimeout(tickTack, interval);
          } else {
            logger.error(`@DDP :: Ping check failure -> [${sessionID}]`);
          }
        })();
      };

      /**
       * Sends ping requests via faye-websocket.
       * @returns {boolean} revealing whether to proceed pinging or not due to ping timeouts
       */
      const sendHeartBeat = () => {
        if (ws) {
          ws.ping('pong', () => {
            const pongStart = new Date().getTime();
            logger.debug(`@DDP :: Ping check succeeded -> [${sessionID}]`);
            return (pongStart - (new Date()).getTime()) < heartbeatTimeout;
          });
        }

        return false;
      };

      keepMeAlive(sendHeartBeat, heartbeatInterval);

      ws.on('message', event => {
        const data = JSON.parse(event.data);

        switch (data.msg) {
          case 'connect':
            logger.info(`@DDP :: Establishing new connection : ${sessionID}`);
            sendMessage({ msg: 'connected', session: sessionID });
            break;
          case 'method':
            if (data.method in methods) {
              logger.info(`@DDP :: Method "${data.method}" is being called.`);
              const result = methods[data.method].apply(this, data.params);

              result
                .then(values => {
                  logger.info(`@DDP :: ${values ? `${values.length} items` : 'No data'} are ` +
                    'being returned');

                  return sendMessage({ msg: 'result', id: data.id, result: values });
                })
                .then(() => sendMessage({ msg: 'updated', methods: [data.id] }))
                .catch(error => {
                  logger.error(`@DDP :: Error calling "${data.method}" : ${error}`);

                  sendMessage({
                    id: data.id,
                    error: {
                      error: 500,
                      reason: 'Internal Server Error',
                      errorType: 'Meteor.Error',
                    },
                  });
                });
            } else {
              logger.error(`@DDP :: Error calling "${data.method}" : not found`);
              sendMessage({
                id: data.id,
                error: {
                  error: 404,
                  reason: 'Method not found',
                  errorType: 'Meteor.Error',
                },
              });
              sendMessage({ msg: 'updated', methods: [data.id] });
            }
            break;
          case 'sub':
            subscriptions[sessionID][data.name] = {
              added: function added(id, doc) {
                return sendMessage({ msg: 'added', collection: data.name, id, fields: doc });
              },
              changed: function changed(id, fields, cleared) {
                return sendMessage({ msg: 'changed', collection: data.name, id, fields, cleared });
              },
              removed: function removed(id) {
                return sendMessage({ msg: 'removed', collection: data.name, id });
              },
            };

            const docs = collections[data.name];

            for (const id in docs) {
              subscriptions[sessionID][data.name].added(id, docs[id]);
            }

            sendMessage({ msg: 'ready', subs: [data.id] });

            break;
          case 'ping':
            sendMessage({ msg: 'pong', id: data.id });
            break;
          default:
        }
      });

      ws.on('close', () => {
        delete subscriptions[sessionID];
        ws = null;
        sessionID = null;
      });
    }
  });

  this.methods = function addMethods(newMethods) {
    for (const key in newMethods) {
      if (key in methods) {
        throw new Error(500, `A method named ${key} already exists`);
      }

      methods[key] = newMethods[key];
    }
  };

  this.publish = function addCollection(name) {
    if (name in collections) {
      throw new Error(500, `A collection named ${name} already exists`);
    }

    const documents = {};
    const proxiedDocuments = {};

    function sendChanged(id, changed, cleared) {
      for (const client in subscriptions) {
        if (subscriptions[client][name]) {
          subscriptions[client][name].changed(id, changed, cleared);
        }
      }
    }

    function change(id, doc) {
      const cleared = [];
      for (const field in documents[id]) {
        if (!(field in doc)) {
          cleared.push(field);
          delete documents[id][field];
        }
      }

      const changed = {};
      for (const field in doc) {
        if (doc[field] !== documents[id][field]) {
          documents[id][field] = changed[field] = doc[field];
        }
      }

      sendChanged(id, changed, cleared);
    }

    function remove(id) {
      delete documents[id];
      for (const client in subscriptions) {
        if (subscriptions[client][name]) {
          subscriptions[client][name].removed(id);
        }
      }
    }

    function add(id, doc) {
      documents[id] = doc;
      proxiedDocuments[id] = new Proxy(doc, {
        set: function set(_, field, value) {
          const changed = {};
          doc[field] = changed[field] = value;
          sendChanged(id, changed, []);
          return value;
        },
        deleteProperty: function deletePropery(_, field) {
          delete doc[field];
          sendChanged(id, {}, [field]);
        },
      });

      for (const client in subscriptions) {
        if (subscriptions[client][name]) {
          subscriptions[client][name].added(id, doc);
        }
      }
    }

    return collections[name] = new Proxy(documents, {
      get: function getDoc(_, id) {
        return proxiedDocuments[id];
      },
      set: function setDoc(_, id, doc) {
        if (documents[id]) {
          change(id, doc);
        } else {
          add(id, doc);
        }
        return proxiedDocuments[id];
      },
      deleteProperty: function deleteDoc(_, id) {
        remove(id);
      },
    });
  };
}

module.exports = DDPServer;
